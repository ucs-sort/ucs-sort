;; This library implements culturally aware string sorting in compliance to ISO/IEC 14651 "International string ordering and comparison -- Method for comparing character strings and description of the common template tailorable ordering"

;; Read the weight table in the factory method and return a closure
;; that implements a ucs-string-lessp function for use in sort

;;parse strings by ordering level, building weights for each collation
;;element. The separator between levels has the weight 0 (the lowest weight)

;;returns as soon as a decision has been taken

(defpackage :ucs-sort
  (:use :cl)
  (:export :create-string-less-function))

(in-package :ucs-sort)

(defun read-weight-table (table-name)
  "Read a pre-calculated weight table from a file and return its
internal representation as a hash map"
  (with-open-file (in table-name)
    (with-standard-io-syntax
      (let
          ((weight-map (make-hash-table :test #'equal))
           (max-length-of-collation-element 0))
        (loop for line = (read in nil) while line
             do
             (progn
               (when (> (length (car line)) max-length-of-collation-element)
                 (setf max-length-of-collation-element (length (car line))))
               (setf (gethash (car line) weight-map)
                     (rest line))))
        (values weight-map max-length-of-collation-element)))))

(defun get-ith-weight (collation-element level weight-map)
  "read the nth-level weight for a given collation element"
  (declare (string collation-element))
  (elt (elt (gethash collation-element weight-map) 0) level))

(defun get-collating-element (str weight-map max-length-of-collation-element)
  "Get the substring belonging to the next collating element in the string, starting from the beginning of the string"
  (let
      ((max-steps
        (min (length str) max-length-of-collation-element)))
    (loop for i from max-steps downto 1 do
         (when (gethash (subseq str 0 i) weight-map)
           (return-from get-collating-element (values (subseq str 0 i)  i))))))

(defun split-into-collating-elements (str weight-map max-length-of-collation-element)
  (declare (string str))
  (when (> (length str) 0)
    (multiple-value-bind (collating-element new-pos)
        (get-collating-element str weight-map max-length-of-collation-element)
      (append (list collating-element)
              (split-into-collating-elements (subseq str new-pos) weight-map max-length-of-collation-element)))))

(defun get-weight (str weight-map max-length-of-collation-element)
  "Calculate the collation weight for a string"
  (declare (string str))
  (declare (ignorable max-length-of-collation-element))
  ;the first, naive version, needs to be greedy
  (let
      ((weight (make-array 20
                           :element-type 'integer
                           :fill-pointer 0
                           :adjustable t))
       (collating-elements ;split the input string into a sequence of
                           ;collating elements
        (split-into-collating-elements str weight-map max-length-of-collation-element)))
    

    (loop for level from 0 upto 3 do
         (loop for collating-element in collating-elements do
              (loop for w across 
                   (get-ith-weight collating-element level weight-map) do
                   (unless (eq w 'ignore)
                     (vector-push-extend w weight))))
         (vector-push-extend 0 weight))
    weight))
         
(defmacro with-hash-table ((key hash-table) &body body)
  "Little helper macro that checks if body has already been calculated
and, if so, extracts it from a hash table"
  `(let
      ((in-table (gethash ,key ,hash-table)))
    (if in-table
        in-table
        (setf (gethash ,key ,hash-table)
              ,@body))))

(defun create-string-less-function (table-name)
  "Takes a table name and returns a closure that functions as a
  ucs-string-lessp for this ordering table. You can use the function
  as predicate in sort and stable-sort"
  (multiple-value-bind 
        (weight-map max-length-of-collation-element)
      (read-weight-table table-name)
    (lambda (s1 s2)
      ;we assume only forward ordering at this point in time
      (block nil
        (let
            ((string-weight-map
              (make-hash-table :test #'equal)))
          (let
              ((weight1 
                (with-hash-table (s1 string-weight-map)
                  (get-weight s1 weight-map max-length-of-collation-element)))
               (weight2
                (with-hash-table (s2 string-weight-map)
                  (get-weight s2 weight-map max-length-of-collation-element))))
                                        ;go through both vectors in parallel and stop as soon an
                                        ;equality has been found
            (map
             'list
             (lambda (w1 w2)
               (cond
                 ((< w1 w2)
                  (return-from nil t))
                 ((> w1 w2)
                  (return-from nil nil))
                 (t  ;continue
                  nil))) weight1 weight2)
            (< (length weight1) (length weight2))))))))

