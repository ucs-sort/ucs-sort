# ucs-sort

This library implements culturally aware string sorting in compliance to ISO/IEC 14651 "International string ordering and comparison -- Method for comparing character strings and description of the common template tailorable ordering". It was originally created to support the implementation of EN 13710 "European Ordering Rules"

It essentially implements a single funciton, create-string-less-function, that reads the weight table in the factory method and return a closure that implements a ucs-string-lessp function for use in sort. The function parses strings by ordering level, building weights for each collation element. The separator between levels has the weight 0 (the lowest weight).

The function returns as soon as a decision has been taken
